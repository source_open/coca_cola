-- phpMyAdmin SQL Dump
-- version 4.0.10.19
-- https://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019-09-19 01:46:11
-- 服务器版本: 5.5.62-log
-- PHP 版本: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `codesceo`
--

-- --------------------------------------------------------

--
-- 表的结构 `mao_data`
--

CREATE TABLE IF NOT EXISTS `mao_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Z_id` varchar(255) DEFAULT '1',
  `user` varchar(20) NOT NULL DEFAULT '',
  `pass` varchar(20) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `gd_gg` text,
  `qq` varchar(15) DEFAULT NULL COMMENT '客服QQ',
  `wx` varchar(20) DEFAULT NULL COMMENT '客服微信',
  `sj` varchar(15) DEFAULT NULL,
  `url` varchar(30) NOT NULL DEFAULT '' COMMENT '系统分发域名',
  `url_1` varchar(30) DEFAULT NULL COMMENT '备用域名',
  `time` varchar(30) NOT NULL DEFAULT '' COMMENT '网站到期时间',
  `dx_1` varchar(1) DEFAULT '1',
  `dx_2` varchar(255) DEFAULT '1',
  `dx_3` varchar(1) DEFAULT '1',
  `dx_4` varchar(1) DEFAULT '1',
  `yzf_type` varchar(1) DEFAULT '1' COMMENT '/0自定义/',
  `yzf_id` varchar(50) DEFAULT NULL,
  `yzf_key` varchar(100) DEFAULT NULL,
  `yzf_url` varchar(100) DEFAULT NULL,
  `zfb_zf` varchar(1) DEFAULT '0',
  `qq_zf` varchar(1) DEFAULT '0',
  `wx_zf` varchar(1) DEFAULT '0',
  `tx_zh` varchar(20) DEFAULT '' COMMENT '提现帐号',
  `tx_sm` varchar(10) DEFAULT NULL COMMENT '提现实名',
  `ym_id` varchar(20) DEFAULT NULL COMMENT '友盟',
  `mzf_id` varchar(20) DEFAULT NULL COMMENT '2',
  `mzf_key` varchar(100) DEFAULT NULL COMMENT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- 转存表中的数据 `mao_data`
--

INSERT INTO `mao_data` (`id`, `Z_id`, `user`, `pass`, `title`, `keywords`, `description`, `price`, `gd_gg`, `qq`, `wx`, `sj`, `url`, `url_1`, `time`, `dx_1`, `dx_2`, `dx_3`, `dx_4`, `yzf_type`, `yzf_id`, `yzf_key`, `yzf_url`, `zfb_zf`, `qq_zf`, `wx_zf`, `tx_zh`, `tx_sm`, `ym_id`, `mzf_id`, `mzf_key`) VALUES
(1, '1', 'admin', '123456', '可乐实物微商城系统', '可乐微商城', '可乐微商城', '99.98', 'cccer.cn ', '2382232787', 'cccer.cn', '123456789', '这几个文字修改为你的网站域名', '这几个文字修改为你的网站备用域名（没有可以不改）', '2099-07-25', '1', '1', '1', '1', '0', '10001', 'xxxxxxxxxxxxxxxxxxxxxxxxxx', 'http://pay.cccer.cn/', '0', '0', '1', '123456@qq.com', '机器猫', '1277743887', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `mao_dindan`
--

CREATE TABLE IF NOT EXISTS `mao_dindan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) NOT NULL DEFAULT '',
  `M_sp` varchar(10) NOT NULL DEFAULT '',
  `ddh` varchar(50) NOT NULL DEFAULT '',
  `sjh` varchar(20) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `sl` varchar(10) NOT NULL DEFAULT '1',
  `dj_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `yf_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `time` varchar(50) NOT NULL DEFAULT '',
  `zt` varchar(1) NOT NULL DEFAULT '1' COMMENT '/1未处理/0已付款(待)/2已处理/',
  `xm` varchar(10) DEFAULT '' COMMENT '收件人',
  `dz` varchar(100) DEFAULT '' COMMENT '收件地址',
  `xxdz` varchar(100) DEFAULT '' COMMENT '详细地址',
  `ly` varchar(30) DEFAULT '',
  `jzxm` varchar(10) DEFAULT '' COMMENT '机主姓名',
  `sfzh` varchar(30) DEFAULT '' COMMENT '机主身份证号',
  `mgz` varchar(255) DEFAULT NULL COMMENT '免冠照',
  `sfz1` varchar(255) DEFAULT NULL COMMENT '身份证正面',
  `sfz2` varchar(255) DEFAULT NULL COMMENT '身份证反面',
  `kdgs` varchar(20) DEFAULT '' COMMENT '快递公司',
  `ydh` varchar(50) DEFAULT NULL COMMENT '运单号',
  `msg` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- 转存表中的数据 `mao_dindan`
--

INSERT INTO `mao_dindan` (`id`, `M_id`, `M_sp`, `ddh`, `sjh`, `name`, `sl`, `dj_price`, `yf_price`, `price`, `time`, `zt`, `xm`, `dz`, `xxdz`, `ly`, `jzxm`, `sfzh`, `mgz`, `sfz1`, `sfz2`, `kdgs`, `ydh`, `msg`) VALUES
(1, '1', '2', '20190725215436923', '17608535320', '【超值捡漏】移动金诚卡', '1', '0.50', '6.00', '6.50', '2019-07-25 21:54:35', '1', '', '', '', '', '', '', NULL, NULL, NULL, '', NULL, NULL),
(2, '1', '3', '20190726103117682', '13164928402', '【人气销量】联通超神卡', '1', '1.00', '6.00', '7.00', '2019-07-26 10:31:17', '1', '', '', '', '', '', '', NULL, NULL, NULL, '', NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `mao_gd`
--

CREATE TABLE IF NOT EXISTS `mao_gd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) NOT NULL DEFAULT '',
  `users` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(1) NOT NULL DEFAULT '',
  `ddh` varchar(50) DEFAULT NULL,
  `kh` varchar(50) DEFAULT NULL,
  `wt` text,
  `img` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `zt` varchar(1) DEFAULT NULL,
  `msg` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000 ;

-- --------------------------------------------------------

--
-- 表的结构 `mao_shop`
--

CREATE TABLE IF NOT EXISTS `mao_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `img` varchar(255) DEFAULT NULL,
  `type` varchar(1) NOT NULL DEFAULT '' COMMENT '1电/2移/3联',
  `tj` varchar(1) NOT NULL DEFAULT '1' COMMENT '0推荐/1默认',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `yf_price` decimal(10,2) DEFAULT '0.00',
  `youhui_zhang` varchar(10) NOT NULL DEFAULT '0',
  `youhui_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `kucun` varchar(10) NOT NULL DEFAULT '0',
  `xiaoliang` varchar(10) NOT NULL DEFAULT '0',
  `beizhu` text,
  `xq` text,
  `slxd_zt` varchar(1) NOT NULL DEFAULT '1' COMMENT '数量下单/0开启/1关闭',
  `rwzl_zt` varchar(1) NOT NULL DEFAULT '1' COMMENT '0开启/1关闭',
  `dqpb` text COMMENT '地区屏蔽',
  `zt` varchar(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- 转存表中的数据 `mao_shop`
--

INSERT INTO `mao_shop` (`id`, `M_id`, `name`, `img`, `type`, `tj`, `price`, `yf_price`, `youhui_zhang`, `youhui_price`, `kucun`, `xiaoliang`, `beizhu`, `xq`, `slxd_zt`, `rwzl_zt`, `dqpb`, `zt`) VALUES
(1, '1', '女童牛仔长裤子春秋款', '/upload/20190725214503592.jpg', '1', '0', '1.00', '6.00', '50', '0.50', '1000', '12', NULL, '商品介绍测试', '0', '0', '', '0'),
(2, '1', '初中学生夏季休闲运动套装', '/upload/20190725215027158.png', '2', '1', '0.50', '6.00', '0', '0.50', '1000', '23', NULL, '商品介绍测试', '1', '1', '', '0'),
(3, '1', '八爪鱼大爆头即食麻辣小海鲜熟食', '/upload/20190725215310806.jpg', '3', '1', '1.00', '6.00', '0', '1.00', '1000', '16', NULL, '商品介绍测试', '1', '1', '', '0');


-- --------------------------------------------------------

--
-- 表的结构 `mao_tx`
--

CREATE TABLE IF NOT EXISTS `mao_tx` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00',
  `time` varchar(50) DEFAULT NULL,
  `zt` varchar(255) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `mao_user`
--

CREATE TABLE IF NOT EXISTS `mao_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) NOT NULL DEFAULT '',
  `users` varchar(50) NOT NULL DEFAULT '',
  `pass` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `mao_wuliu`
--

CREATE TABLE IF NOT EXISTS `mao_wuliu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `M_id` varchar(10) NOT NULL DEFAULT '',
  `users` varchar(50) DEFAULT NULL,
  `ddh` varchar(50) DEFAULT NULL,
  `msg` text,
  `time` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
